#!/usr/bin/env sh

mkdir -p /etc/nginx/http.d/
if [ ! -f /etc/nginx/ssl/live/test.clarin-dev.eu/fullchain.pem ]; then
  echo "No letsencrypt certificate found, enabling bootstrap vhost"
  cp /vhosts/bootstrap/* /etc/nginx/http.d/
else
  echo "Letsencrypt certificate found, enabling ssl vhost"
  cp /vhosts/ssl/* /etc/nginx/http.d/

  #Generate dhparam.pem if needed
  if [ -f /etc/nginx/ssl-extra/dhparam.pem ]; then
    echo "Skipping generation of dhparam.pem file, already exists"
  else
    echo 'Generating dhparam.pem file'
    openssl dhparam -out /etc/nginx/ssl-extra/dhparam.pem 4096
  fi
fi