Docker compose configuration to bootstrap and run an nginx container to demonstrate the
interaction with letsencrypt and testing various configurations.

# Bootstrapping

Start webserver:
```
docker-compose up -d webserver
```

Issue new certificate:
```
docker-compose run certbot \
    certonly \
    --webroot \
    --webroot-path /var/www/certbot/ \
    --test-cert \
    -d test.clarin-dev.eu \
    -m test@test.com
```

Restart webserver:
```
docker-compose up -d --force-recreate webserver
```

SSL setup, with OCSP enabled, should now be running as expected. Please not that in this example
we request a test (i.e. signed by untrusted ca) certificate.